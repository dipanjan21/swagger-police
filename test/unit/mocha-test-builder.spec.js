'use strict';

const requireWithMocks = require('proxyquire').noCallThru().noPreserveCache();

describe('unit/mocha-test-builder', () => {

    const Hooks = require('../../lib/hooks');
    
    let testBuilder;
    let requestPromiseSpy;

    beforeEach(() => {

        requestPromiseSpy = jasmine.createSpy('requestPromise');

        testBuilder = requireWithMocks('../../lib/mocha-test-builder', {
            'request-promise-native': requestPromiseSpy
        });
    });

    describe('buildUri', () => {

        let serverBaseUrl;
        let testCase;

        beforeEach(() => {
            serverBaseUrl = 'https://localhost:8080';
            testCase = {
                name: 'GET /{path-name}/services/{name} -> 200',
                basePath: '/v1',
                request: {
                    path: '/{path-name}/services/{name}',
                    method: 'get',
                    pathParams: {
                        name: 'test-service-name',
                        'path-name': 'test-path-name'
                    },
                    query: {}
                },
                response: {
                    statusCode: 200
                }
            };
            requestPromiseSpy.and.returnValue(Promise.resolve({statusCode: 200}));
        });

        it('builds the uri by substituting the path params', (done) => {

            const testSuiteRunner = testBuilder.buildTestSuite({
                testCases: [testCase],
                hooks: new Hooks(),
                serverBaseUrl
            }).run();

            testSuiteRunner.on('end', () => {
                expect(requestPromiseSpy).toHaveBeenCalledWith(jasmine.objectContaining({
                    uri: `${serverBaseUrl}/v1/test-path-name/services/test-service-name`
                }));
                done();
            });
        });

        it('builds the uri properly when the path params are empty', (done) => {

            const testSuiteRunner = testBuilder.buildTestSuite({
                testCases: [testCase],
                hooks: new Hooks(),
                serverBaseUrl
            }).run();

            testCase.request.path = '/services';
            testCase.request.pathParams = {};

            testSuiteRunner.on('end', () => {
                expect(requestPromiseSpy).toHaveBeenCalledWith(jasmine.objectContaining({
                    uri: `${serverBaseUrl}/v1/services`
                }));
                done();
            });
        });

        it('builds the uri properly when the path params are undefined', (done) => {

            const testSuiteRunner = testBuilder.buildTestSuite({
                testCases: [testCase],
                hooks: new Hooks(),
                serverBaseUrl
            }).run();

            testCase.request.path = '/services';
            testCase.request.pathParams = undefined;

            testSuiteRunner.on('end', () => {
                expect(requestPromiseSpy).toHaveBeenCalledWith(jasmine.objectContaining({
                    uri: `${serverBaseUrl}/v1/services`
                }));
                done();
            });
        });

        it('builds the uri properly when base url is missing', (done) => {
            testCase.basePath = undefined;
            const testSuiteRunner = testBuilder.buildTestSuite({
                testCases: [testCase],
                hooks: new Hooks(),
                serverBaseUrl
            }).run();

            testSuiteRunner.on('end', () => {
                expect(requestPromiseSpy).toHaveBeenCalledWith(jasmine.objectContaining({
                    uri: `${serverBaseUrl}/test-path-name/services/test-service-name`
                }));
                done();
            });
        });

        it('builds the uri properly when base url is empty', (done) => {
            testCase.basePath = '';
            const testSuiteRunner = testBuilder.buildTestSuite({
                testCases: [testCase],
                hooks: new Hooks(),
                serverBaseUrl
            }).run();

            testSuiteRunner.on('end', () => {
                expect(requestPromiseSpy).toHaveBeenCalledWith(jasmine.objectContaining({
                    uri: `${serverBaseUrl}/test-path-name/services/test-service-name`
                }));
                done();
            });
        });

        it('builds the uri properly when base url is /', (done) => {
            testCase.basePath = '/';
            const testSuiteRunner = testBuilder.buildTestSuite({
                testCases: [testCase],
                hooks: new Hooks(),
                serverBaseUrl
            }).run();

            testSuiteRunner.on('end', () => {
                expect(requestPromiseSpy).toHaveBeenCalledWith(jasmine.objectContaining({
                    uri: `${serverBaseUrl}/test-path-name/services/test-service-name`
                }));
                done();
            });
        });

    });
});
