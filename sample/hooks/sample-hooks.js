hooks.add('GET /pet/{petId} -> 200', {
    before: (testCase, done) => {
        done();
    },
    
    after: (testCase, done) => {
        done();
    }
});

hooks.add('GET /pet/{petId} -> 200 # Pass 2', {
    before: (testCase, done) => {
        done();
    },

    after: (testCase, done) => {
        done();
    }
});

hooks.beforeEach((testcase, done) => {
    done();
});

hooks.afterEach((testcase, done) => {
    done();
});

hooks.beforeAll((testcases, done) => {
    done();
});

hooks.afterAll((testcases, done) => {
    done();
});
