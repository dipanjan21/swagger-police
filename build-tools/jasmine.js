#!/usr/bin/env node

var path = require('path');
var Command = require('jasmine/lib/command');
var Jasmine = require('jasmine');
var JasmineConsoleReporter = require('jasmine-console-reporter');
var JUnitXmlReporter = require('jasmine-reporters').JUnitXmlReporter;

var projectBaseDir = path.resolve(__dirname + '/..');
var jasmine = new Jasmine({
    projectBaseDir: projectBaseDir
});
var command = new Command(projectBaseDir);

jasmine.addReporter(new JUnitXmlReporter({
    savePath: projectBaseDir + '/build-output/test-reports',
    filePrefix: 'junit-report',
    consolidate: true,
    useDotNotation: true
}));

jasmine.addReporter(new JasmineConsoleReporter({
    colors: true,
    cleanStack: 1,       // (0|false)|(1|true)|2|3
    verbosity: 4,        // (0|false)|1|2|3|(4|true)
    listStyle: 'indent', // 'flat'|'indent'
    activity: false
}));

jasmine.onComplete(function (passed) {
    process.exit(passed ? 0 : 1);
});

command.run(jasmine, process.argv.slice(2));
