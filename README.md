**swagger-police** is a command line tool for validating backend APIs against the published swagger specification. It can 
be plugged into a continuous integration system to ensure that the backend APIs confirms to the behaviour mentioned
in the published swagger specification.

This library is very similar to and inspired from abao (https://github.com/cybertk/abao) which does similar validations for a RAML spec

**Please Note:** The library is still in development !

# Features #

* Calls every API defined in the swagger file by generating mock data from the specs (data can be customised, see usage). 
This ensures that the url params, query params, headers and the body defined in the spec is supported by the service.
* Verifies the HTTP response status against the specs.
* Verifies the response body against the schema defined in specs (JSON schema validation).
* Supports the following hook methods for customising the tests (see Hooks)
  * BeforeAll
  * AfterAll
  * BeforeEach
  * AfterEach
  * Test specific *Before* and *After*
  
# Limitations #
  
* Response headers are not validated against the schema
* Only JSON and form posts are supported in request body
* Only JSON responses will be validated against a schema

# Installation #

`npm install -g swagger-police`

# Usage #

```
$ swagger-police

  Usage: swagger-police <swagger URL/path> [options]

  Options:

    -h, --help                        output usage information
    --server [server]                 The API endpoint
    --hook-files [hookFiles]          Specify pattern to match hook files
    --testcase-names [testcaseNames]  Print all the testcase names (does not execute the tests)

```

* **Swagger URL/path** - The path or the URL to the swagger file
* **Server** - The API endpoint base url. No need to specify this if a swagger url is specified and the APIs are hosted in the same server.
* **Hook Files** - A glob pattern (reletive to the execution directory) to load the hook files.

# Mock Data #

The mock data for the requests are generated using the [swagmock](https://www.npmjs.com/package/swagmock) package. But it
can be customised by either of the following ways

* The request data can be customised in the testcase specific before hook by modifying the `testcase.request` object. 
See Testcase Object Format for the parameter names and values.
* Swagger does not provide a way to specify examples in request parameters yet, but allows vendor specific extensions. The
tool will look for `x-example` property in the `parameter` and will use that instead.

```
"post": {
    "description": "Creates a new pet in the store.  Duplicates are allowed",
    "operationId": "addPet",
    "produces": [
        "application/json"
    ],
    "parameters": [
        {
            "name": "pet",
            "in": "body",
            "description": "Pet to add to the store",
            "required": true,
            "schema": {
                "$ref": "#/definitions/newPet"
            },
            "x-example": {
                "id": 0,
                "category": {
                    "id": 0,
                    "name": "dog"
                },
                "name": "doggie",
                "photoUrls": [
                    "http://doggie.com/avatar"
                ],
                "tags": [
                    {
                        "id": 0,
                        "name": "string"
                    }
                ],
                "status": "available"
            }
        }
    ],
    "responses": {
        "200": {...
```

# Hooks #

The tool supports the following test hooks to enable setup/tear-down tasks or customising
the individual tests. Hooks are simple JavaScript files which have access to a global `hooks` object with methods to add the specific hooks.

## `BeforeAll` and `AfterAll` ##

These will be executed once before the tests start and after all the tests have been
executed. Note that only one of each type can be specified, there cannot be more than one beforeAll/afterAll hooks. However, 
testcase specific hooks can be specified, see below.

```
hooks.beforeAll((testcases, done) => {
    done();
});

hooks.afterAll((testcases, done) => {
    done();
});
```

* **testcases** - An array of `testcase` objects to be executed. This is generated from the swagger specs. Any 
changes made to the testcase objects in the `beforeAll` hook will be reflected in the tests.
* **done** - The callback function

## `BeforeEach` and `AfterEach` ##

These will be executed before and after every test. Note that only one of each type can be specified, there cannot be 
more than one beforeEach/afterEach hooks. However, testcase specific before/after hooks can be specified, see below.

```
hooks.beforeEach((testcase, done) => {
    done();
});

hooks.afterEach((testcase, done) => {
    done();
});
```

* **testcase** - The specific `testcase` this hook is being executed for. Any
changes made to the testcase objects in the `beforeEach` hook will be reflected in the tests.
* **done** - The callback function

## Testcase specific hooks ##

`before` and `after` testcase specific hooks can be specified which will only be executed before and after the
specific testcase. The testcases are identified using a generated name. Run the tool with the `--testcase-names` 
option to print out all the testcase names.

The hooks can be specified using the following method

```
hooks.add('GET /pet/{petId} -> 200', {
    before: (testcase, done) => {
        done();
    },
    
    after: (testcase, done) => {
        done();
    }
});
```

* **1st Argument** - The testcase name. Please note that this is case sensitive. 
* **2nd argument** - An object with before and after functions which takes in testcase (The testcase object 
representing the specific test) and a callback. Any changes made to the testcase object will reflect in the test.

If more than one such hook is specified for a specific test, the test will be executed once for every hook specified. 
Custom test name can be added to identify each pass. See below

```
hooks.add('GET /pet/{petId} -> 200 # Pass 1', {
    before: (testCase, done) => {
        done();
    },
    
    after: (testCase, done) => {
        done();
    }
});

hooks.add('GET /pet/{petId} -> 200 # Pass 2', {
    before: (testCase, done) => {
        done();
    },

    after: (testCase, done) => {
        done();
    }
});
```

# Testcase Object Format #

## Example

```
{
  "name": "GET /pets -> 200",
  "basePath": "/api",
  "request": {
    "path": "/pets/{id}",
    "method": "get",
    "pathParams": {
      "id": 4948307189170176
    },
    "query": {
      "tags": "EhuDzn",
      "limit": -4836393545105408
    },
    "headers": {
        "x-ample-header": "value"
    },
    "body": {}
  },
  "response": {
    "statusCode": 200,
    "schema": {
      "type": "array",
      "items": {
        "type": "object",
        "required": [
          "id",
          "name"
        ],
        "properties": {
          "id": {
            "type": "integer",
            "format": "int64"
          },
          "name": {
            "type": "string"
          },
          "tag": {
            "type": "string"
          }
        }
      }
    }
  }
}

```

* **request** - contains the data with which the API call will be made. Modify these values in `before` hook if the request 
needs to be customised
* **response** - contains the expected response data like the status code, schema & response headers