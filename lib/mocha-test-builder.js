'use strict';

const _ = require('lodash');
const assert = require('assert');
const Mocha = require('mocha');
const requestPromise = require('request-promise-native');
const tv4 = require('tv4');
const url = require('url');

function buildUri(baseUrl, basePath, request) {

    let path = request.path;

    if (basePath && basePath !== '/') {
        path = basePath + request.path;
    }

    if (!_.isEmpty(request.pathParams)) {
        _.entries(request.pathParams).forEach(([key, value]) => {
            path = path.replace(`{${key}}`, value);
        });
    }

    return url.resolve(baseUrl, path);
}

function prettyJSON(obj) {
    return JSON.stringify(obj, null, 2);
}


function invokeSuiteHooks({testCases, globalHooks, suite}) {

    if (globalHooks.beforeAll) {
        suite.beforeAll((done) =>  globalHooks.beforeAll(testCases, done));
    }

    if (globalHooks.afterAll) {
        suite.afterAll((done) =>  globalHooks.afterAll(testCases, done));
    }
}


function invokeTestcaseHooks({testCase, globalHooks, testCaseHook, suite}) {

    if (globalHooks.beforeEach) {
        suite.beforeEach((done) =>  globalHooks.beforeEach(testCase, done));
    }

    if (globalHooks.afterEach) {
        suite.afterEach((done) =>  globalHooks.afterEach(testCase, done));
    }

    if (testCaseHook.before) {
        suite.beforeEach((done) => testCaseHook.before(testCase, done));
    }

    if (testCaseHook.after) {
        suite.afterEach((done) => testCaseHook.after(testCase, done));
    }
}

function createMochaSuite({parentSuite, testCase, hooks, serverBaseUrl}) {

    const globalHooks = hooks.globalHooks;
    const testCaseHooks = hooks.testCaseHooksMap[testCase.name] || [{}];
    const hooksCount = testCaseHooks.length;

    testCaseHooks.forEach((testCaseHook, index) => {

        const suiteName = testCase.name + (hooksCount > 1 ? ` # ${testCaseHook.name || (index+1)}`:'');
        const suite = Mocha.Suite.create(parentSuite, suiteName);

        invokeTestcaseHooks({
            testCase,
            globalHooks,
            testCaseHook,
            suite
        });

        suite.addTest(new Mocha.Test('Verifying API', (done) => {

            const request = testCase.request;

            const options = {
                uri: buildUri(serverBaseUrl, testCase.basePath, request),
                method: request.method,
                qs: request.query,
                headers: request.headers,
                simple: false,
                resolveWithFullResponse: true
            };

            if (request.body) {
                options.body = request.body;
                options.json = true;
            } else if (request.form) {
                options.form = request.form;
            }

            requestPromise(options)
                .then((response) => {
                    const expectedResponse = testCase.response;

                    assert.equal(
                        response.statusCode,
                        expectedResponse.statusCode,
                        `Status Code Mismatch: Expected ${expectedResponse.statusCode}, got ${response.statusCode}`
                    );

                    if (!_.isEmpty(expectedResponse.headers)) {
                        // TODO: add response header validations
                    }

                    if (! _.isEmpty(expectedResponse.schema)) {
                        const responseData = JSON.parse(response.body);
                        const result = tv4.validateMultiple(responseData, expectedResponse.schema);
                        assert(
                            result.valid,
                            `${prettyJSON(result.errors)}
                             ${prettyJSON(result.missing)}
                            `
                        );
                    }

                    done();
                })
                .catch(done);
        }));
    });
}

function buildTestSuite({testCases, hooks, serverBaseUrl}) {

    const mocha = new Mocha();
    const parentSuite = Mocha.Suite.create(mocha.suite, 'Swagger API Tests');

    invokeSuiteHooks({
        testCases,
        globalHooks: hooks.globalHooks,
        suite: parentSuite
    });
    testCases.forEach((testCase) => createMochaSuite({parentSuite, testCase, hooks, serverBaseUrl}));

    return mocha;
}

module.exports = {
    buildTestSuite
};