'use strict';

const SwaggerParser = require('swagger-parser');
const testCaseFactory = require('./testcase-factory');
const mochaTestBuilder = require('./mocha-test-builder');
const hooksParser = require('./hooks-parser');
const _ = require('lodash');

function getServerBaseUrl({programOptions, swaggerPath, swaggerObject}) {

    if (programOptions.server) {
        return programOptions.server;
    }

    if (swaggerObject.host && !_.isEmpty(swaggerObject.schemes)) {
        return `${swaggerObject.schemes[0]}://${swaggerObject.host}`;
    }

    return swaggerPath; 
}

function runTests({programOptions, swaggerPath, swaggerObject}) {
    return Promise.resolve()
        .then(() => testCaseFactory.getTestCases(swaggerObject))
        .then((testCases) => {
            const hooks = hooksParser.parse(programOptions.hookFiles);
            const serverBaseUrl = getServerBaseUrl({programOptions, swaggerObject, swaggerPath});

            return mochaTestBuilder.buildTestSuite({testCases, hooks, serverBaseUrl});
        })
        .then((mochaTestSuite) => {
            mochaTestSuite.run((failures) => {
                process.exit(failures);  // exit with non-zero status if there were failures
            });
        });
}

function printTests({swaggerObject}) {
    return Promise.resolve()
        .then(() => testCaseFactory.getTestCases(swaggerObject))
        .then((testCases) => testCases.forEach((testCase) => console.log(`${testCase.name}`)));
}

function runCommand(programOptions) {

    return (swaggerPath) => {
        const swaggerParser = new SwaggerParser();

        return Promise.resolve()
            .then(() => swaggerParser.validate(swaggerPath))
            .then((swaggerObject) => {
                if (programOptions.testcaseNames) {
                    return printTests({swaggerObject});
                }

                return runTests({programOptions, swaggerPath, swaggerObject});
            })
            .catch((error) => {
                console.log(error);
            });
    };
}

module.exports = {
    runCommand
};