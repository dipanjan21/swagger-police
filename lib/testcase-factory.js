'use strict';

const _ = require('lodash');
const SwagMock = require('swagmock');

function generateSampleData({param, mockParams}) {

    const example = param['x-example'];

    if (example) {
        return example;
    }
    
    const mock = mockParams.parameters[param.in].find(({name}) => name === param.name);
    return mock.value;
}

function parseRequestParams({mockGen, path, operation, operationObj}) {

    const requestParams = {
        pathParams: {},
        query: {},
        headers: {}
    };

    if (!operationObj.parameters) {
        return Promise.resolve(requestParams);
    }

    return Promise.resolve()
        .then(() => mockGen.parameters({path, operation}))
        .then((mockParams) => {

            operationObj.parameters.forEach((param) => {
                const sampleData = generateSampleData({param, mockParams});

                switch (param.in) {
                case 'query':
                    requestParams.query[param.name] = sampleData;
                    break;

                case 'header':
                    requestParams.headers[param.name] = sampleData;
                    break;

                case 'path':
                    requestParams.pathParams[param.name] = sampleData;
                    break;

                case 'body':
                    requestParams.body = sampleData;
                    break;

                case 'formData':
                    requestParams.form = sampleData;
                    break;
                }
            });

            return requestParams;
        });
}

function parseOperation({swaggerObject, path, operation, operationObj}) {

    const mockGen = SwagMock(swaggerObject, {validated: true});

    return Promise.all(_.entries(operationObj.responses)
        .map(([statusCode, responseObj]) => {

            return Promise.resolve()
                .then(() => parseRequestParams({mockGen, path, operation, operationObj}))
                .then((requestParams) => {
                    const request = Object.assign(
                        {
                            path,
                            method: operation
                        },
                        requestParams
                    );

                    return {
                        name: `${operation.toUpperCase()} ${path} -> ${statusCode}`,
                        basePath: swaggerObject.basePath,
                        request,
                        response: {
                            statusCode: Number(statusCode),
                            schema: responseObj.schema,
                            headers: responseObj.headers
                        },
                        swaggerObject
                    };
                });
        })
    );
}

function parsePath({swaggerObject, path, pathObj}) {

    return Promise.resolve(_.entries(pathObj))
        .then((entries) => Promise.all(
            entries.map(([operation, operationObj]) => parseOperation({swaggerObject, path, operation, operationObj}))
        ))
        .then(_.flatMap);
}

function getTestCases(swaggerObject) {

    return Promise.resolve(_.entries(swaggerObject.paths))
        .then((entries) => Promise.all(entries.map(([path, pathObj]) => parsePath({swaggerObject, path, pathObj}))))
        .then(_.flatMap);
}

module.exports = {
    getTestCases
};
