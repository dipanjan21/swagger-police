'use strict';

const _ = require('lodash');

function parseTestCaseName(name) {
    return name.split('#').map(_.trim);
}

function Hooks() {

    this.testCaseHooksMap = {};
    this.globalHooks = {};

    this.add = (name, {before, after}) => {
        const [testCaseName, hookName] = parseTestCaseName(name);
        const testCaseHooks = this.testCaseHooksMap[testCaseName] || [];
        
        testCaseHooks.push({name: hookName, before, after});
        this.testCaseHooksMap[testCaseName] = testCaseHooks;
    };

    this.beforeAll = (hook) => this.globalHooks.beforeAll = hook;

    this.afterAll = (hook) => this.globalHooks.afterAll = hook;

    this.beforeEach = (hook) => this.globalHooks.beforeEach = hook;

    this.afterEach = (hook) => this.globalHooks.afterEach = hook;
}

module.exports = Hooks;