
const commandFactory = require('./command-factory');
var program = require('commander');

function run() {
    program
        .usage('<swagger URL/path> [options]')
        .option('--server [server]', 'The API endpoint')
        .option('--hook-files [hookFiles]', 'Specify pattern to match hook files')
        .option('--testcase-names [testcaseNames]', 'Print all the testcase names (does not execute the tests)');
    
    program
        .arguments('<swaggerPath>')
        .action(commandFactory.runCommand(program));

    program.parse(process.argv);
    
    if (!process.argv.slice(2).length) {
        program.help();
    }
}

module.exports = run;
