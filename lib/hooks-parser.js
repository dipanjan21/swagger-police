'use strict';

const Hooks = require('./hooks');
const path = require('path');
const glob = require('glob');

function parse(hooksPattern) {
    const hooks = new Hooks();
    
    if (!hooksPattern) {
        return hooks;
    }

    const hookFiles = glob.sync(hooksPattern);
    global.hooks = hooks;

    if (hookFiles.length !== 0) {
        hookFiles.forEach((file) => {
            require(path.resolve(process.cwd(), file));
        });
    }
    
    return hooks; 
}

module.exports = {
    parse
};
